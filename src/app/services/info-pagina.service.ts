import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoEquipo, InfoPagina } from '../../interfaces/info-pagina-interface';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {

  info: InfoPagina = {};
  equipo: InfoEquipo = {};
  cargada = false;

  constructor( private http: HttpClient ) {

    this.cargarInfo();
    this.cargarEquipo();

  }

  private cargarInfo() {
    // LEER el archivo JSON
    this.http.get('assets/data/data-page.json')
        .subscribe( (resp: InfoPagina) => {

          this.cargada = true;
          this.info = resp;
          console.log('Servicio de infoPagina listo');
          console.log(resp);
        });
  }

  private cargarEquipo() {
    // LEER el archivo JSON
    this.http.get('https://angular-portafolio-a0b3e.firebaseio.com/equipo.json')
    .subscribe( (resp: InfoEquipo) => {

      this.cargada = true;
      this.equipo = resp;
      console.log('Servicio de infoEquipo listo');
      console.log(resp);
    });
  }
}
